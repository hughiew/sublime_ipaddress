import sublime, sublime_plugin
import urllib  
import urllib2  
import threading 

class IpaddressCommand(sublime_plugin.TextCommand):
	def run(self, edit): 
		thread = IPAddressFetch(5)  
		thread.start()  

		edit = self.view.begin_edit('ipaddress')  
		self.handle_thread(edit, thread)  

	def handle_thread(self, edit, thread, i=0, dir=1): 
		if thread.is_alive(): 
			self.animate_activity(thread, edit, i, dir)   
			return
		if thread.result == False:
			self.animate_activity(thread, edit, i, dir)  
			return

		self.replace(edit, thread)

	def animate_activity(self, thread, edit, i=0, dir=1): 
		before = i % 8  
		after = (7) - before  
		if not after:  
			dir = -1  
		if not before:  
			dir = 1  
		i += dir  
		self.view.set_status('ipaddress', 'Fetching IP Address [%s=%s]' % (' ' * before, ' ' * after))  
		sublime.set_timeout(lambda: self.handle_thread(edit, thread, i, dir), 100)  
		return

	def replace(self, edit, thread, offset=0):
		sels = self.view.sel()  
		result = thread.result  
		for sel in sels:  
			sel = sublime.Region(sel.begin() + offset, sel.end() + offset)   
			self.view.replace(edit, sel, result)   

		self.view.erase_status('ipaddress')  
		self.view.end_edit(edit)  

class IPAddressFetch(threading.Thread):  
	def __init__(self, timeout):  
		self.timeout = timeout  
		self.result = None  
		threading.Thread.__init__(self)  
  
	def run(self):  
		try:  
			request = urllib2.Request('http://hughiew.co.uk/ip.php')  
			http_file = urllib2.urlopen(request, timeout=self.timeout)  
			self.result = http_file.read()  
			return  
  
		except (urllib2.HTTPError) as (e):  
			err = '%s: HTTP error %s contacting API' % (__name__, str(e.code))  
		except (urllib2.URLError) as (e):  
			err = '%s: URL error %s contacting API' % (__name__, str(e.reason))  
  
		sublime.error_message(err)  
		self.result = False  